#include "Player.h"
#include "Factory.h"

using namespace std;

Bolt* Player::AskFactory(const Token &tocken)
{
	return nullptr;
}

void Player::UseBolt() {
	if (m_bolts.empty()) {
		WLOG("No bolts avaible.");
		m_bolts.push(make_shared<Bolt>(
				this->AskFactory(Token::DEFAULT)
			)
		);
	}


}

void Player::Update(float dt)
{

}
