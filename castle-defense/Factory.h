#pragma once
#include "Bolts.h"
#include "Player.h"

namespace Factory 
{

	struct BoltFactory {
		virtual Bolts::Bolt* Create(float area, float dmg, unsigned short time) = 0;
		virtual ~BoltFactory() {}
	};

	struct DefaultBoltFactory : public BoltFactory {
		Bolts::Bolt* Create(float area, float dmg, unsigned short time) override {
			Bolts::Bolt* pBolt = nullptr;
			try {
				pBolt = new DefaultBolt(area, dmg);
			}
			catch (std::bad_alloc&ex) {
				ELOG(ex.what());
			}
			return pBolt;
		};
		virtual ~DefaultBoltFactory() {}
	};

	struct TimeBoltFactory : public BoltFactory {
		Bolts::Bolt* Create(float area, float dmg, unsigned short time) override {
			Bolts::Bolt* pBolt = nullptr;
			try {
				pBolt = new TimeBolt(area, dmg, time);
			}
			catch (std::bad_alloc&ex) {
				ELOG(ex.what());
			}
			return pBolt;
		};
		virtual ~TimeBoltFactory() {}
	};


	extern Bolts::Bolt* AskFactory(const Bolts::BoltData& data, const Player* player);

}