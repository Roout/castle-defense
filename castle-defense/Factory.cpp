#include "Factory.h"

Bolts::Bolt * Factory::AskFactory(const Bolts::BoltData & data, const Player * player)
{
	Bolts::Bolt* pBolt = nullptr;
	shared_ptr<BoltFactory> pFactory = nullptr;

	if (data._token == Token::TIME) {
		pFactory = make_shared<TimeBoltFactory>();
	}
	else {
		pFactory = make_shared<DefaultBoltFactory>();
	}
	pBolt = pFactory->Create(data._area, data._dmg, data._delay);
	return pBolt;
}
