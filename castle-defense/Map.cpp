#include "Map.h"

Cell::Cell(int row, int col) :
	m_row(row),
	m_col(col)
{
	assert(MapHelper::IsWithinMap(m_row, m_col));
}

void Cell::AddObject(Object * pObj)
{
	m_pVisitor.push_back(pObj);
}

void Cell::RemoveObject(Object * pObj)
{
	m_pVisitor.remove(pObj);
}


void Cell::Update(float dt)
{
	Object::Update(dt);
}

void Cell::Outbreak(const Status * const pStatus)
{
	if (pStatus->IsInfection())
		this->Spread(pStatus);
	else
		this->Affect(pStatus);
}

void Cell::Expire( Status *  pStatus)
{
	this->RemoveStatus(pStatus);
}

std::string Cell::About() const
{
	std::string about = "Cell: [ " + std::to_string(m_row) + " , "
		+ std::to_string(m_col) + " ]. Statuses(" 
		+ std::to_string(m_pStatus .size())+"): ";

	for (auto& s : m_pStatus) {
		about.append(s->About() + ",");
	}
	return about;
}

void Cell::Apply(const Status * const)
{
	//empty: nothing can do to cell
}

std::pair<int, int> Cell::GetCoordinates() const
{
	return std::make_pair(m_row,m_col);
}

void Cell::Affect(const Status * const pStatus)
{
	ILOG( "Affected" );
	for (auto & pVisitor : m_pVisitor) {
		pVisitor->Apply(pStatus);
	}
}

void Cell::Spread(const Status * const pStatus)
{
	ILOG("Spreaded");
	for (auto & pVisitor : m_pVisitor) {
		Status* status = pStatus->Clone();
		pVisitor->AddStatus(status);
		assert(status == nullptr);
	}
}

bool MapHelper::IsWithinMap (int row, int col)
{
	int		 mxRows = static_cast<uint32_t>(Map::Size::rows),
			 mxCols = static_cast<uint32_t>(Map::Size::cols);

	return row < mxRows && col < mxCols && row >= 0 && col >= 0;
}

uint32_t MapHelper::CalculateDistance(int r1, int c1, int r2, int c2) {
	return uint32_t( sqrt( pow(r2-r1,2) + pow(c2-c1,2) ) );
}

