#pragma once
#include "Logger.h"

namespace Bolts 
{
	enum class Token {
		DEFAULT	= 1 << 0, // default with 3 sec delay
		TIME	= 1 << 1, // explode with custom delay
		FREEZE	= 1 << 2, // freeze in aoe
		FIRE	= 1 << 3, // create sea of fire
		LAND	= 1 << 4,

	};
	
	struct BoltData {
		Token	_token;
		float	_area,
				_dmg;
		unsigned short 
				_delay;
	};

	class Bolt {
		public:
			Bolt(float area,float dmg) :
				_area(area),
				_damage(dmg) {}

			virtual ~Bolt() {}

			virtual void Update(float dt) = 0;

			virtual const char* About()const = 0;

			virtual void Activate() = 0;

		protected:

			virtual void Explode() = 0;

			//main properties
			float _area;
			float _damage;
		};
	
	class LandBolt : public Bolt {
	public:
		LandBolt(float area, float dmg) :
			Bolt(area, dmg),
			_active(false)
		{}

		void Update(float dt) override {
			//nothing now
		}

		const char* About() const override {
			return "Explode after someone step on it";
		};

		void Activate() override {
			_active = true;
		}

		bool IsActive() const {
			return _active;
		}

	protected:

		void Explode() override {
			ILOG("LandBolt exploded");
			_active = false;
		}

		bool	_active;
	};
	class DefaultBolt : public Bolt {
	public:
		static const unsigned short _delay = 3;

		DefaultBolt(float area, float dmg, unsigned short time = _delay) :
			Bolt(area, dmg),
			_timer(static_cast<float> (time)),
			_active(false) 
		{}

		void Update(float dt) override {
			if (_timer <= 0.f || !_active) return;

			_timer -= dt;
			if (_timer <= 0.f) this->Explode();
		}

		const char* About() const override {
			return "Explode after 3 seconds";
		};
		
		void Activate() override {
			_active = true;
		}

		bool IsActive() const {
			return _active;
		}

	protected:

		void Explode() override {
			ILOG("TimeBolt exploded.")
			_active = false;
		}
		
		float	_timer;
		bool	_active;
		
	};

	class TimeBolt : public DefaultBolt {
	public:
		
		TimeBolt(float area, float dmg, unsigned short time = _delay) :
			DefaultBolt(area, dmg, time) 
		{
			_timer = time > _mxDelay ? _mxDelay : time;
			_timer = time < _mnDelay ? _mnDelay : time;
		}

		const char* About() const override {
			return "Explode after custom time";
		};

		void TimeUp() {
			_timer ++;
			///TODO: change this extra-operations to smth cool
			_timer = _timer > _mxDelay ? _mxDelay : _timer;
		}

		void TimeDown() {
			_timer --;
			///TODO: change this extra-operations to smth cool
			_timer = _timer < _mnDelay ? _mnDelay : _timer;
		}

	private:
		static const unsigned short
			_mxDelay = 15,
			_mnDelay = 0;

	};
}



