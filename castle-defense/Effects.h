#pragma once
#include "Map.h"

namespace BoltEffect 
{
	namespace UniqueStatus
	{
		class BurningStatus : public Status {
		public:

			BurningStatus(int time, float cd, float dmg, bool isInfection);

			std::string About() const override;
		};
	}
	
	class Explosion {
	public:

		Explosion(uint32_t radius, float dmgInCntr);

		/*
			Spread status on cells in square shape:

			EXample: radius = 3; Epicenter in Cell(4,4)

			  1 2 3 4 5 6 7
			1 * * * * * * *
			2 * * * S * * *
			3 * * S S S * *
			4 * S S S S S *
			5 * * S S S * *
			6 * * * S * * *

		*/
		virtual void Start(std::shared_ptr<Cell>& m_pCell, Map&map);

	protected:
		// maximal radius of explosion
		// count of cells
		uint32_t m_radius;
		float	 m_damageInEpicenter;

		virtual float CalculateDamage(uint32_t targetRange) const;

		virtual Status* CreateStatus(uint32_t targetRange) const  = 0;
	};	

	class FireExplosion : public Explosion {
	public:

		FireExplosion(uint32_t radius, float dmgInCntr, float duration);

		void Start(std::shared_ptr<Cell>& m_pCell, Map&map) override;

	private:

		float m_duration;

	protected:

		float CalculateDamage(uint32_t targetRange) const override;

		Status* CreateStatus(uint32_t targetRange) const override ;
	};
}