#pragma once
#include "Logger.h"
#include <list>
#include <string>
#include <memory>
#include <boost\assert.hpp>

namespace Core 
{
	using std::list;

	typedef float t_Percents;
	
	class Status;
	//Observer
	class Object {
	public:
		virtual ~Object() {}

		virtual void Update(float dt) ;
		// Observer part 
		// [fn being called on notification from Observable]
		virtual void Outbreak(const Status*const) = 0;
		// Observer part 
		// [fn being called on notification from Observable]
		virtual void Expire(Status* pStatus) = 0;
		// <code> auto data = pStatus->GetData(); </code>
		// do with data whatever you want
		virtual void Apply(const Status * const pStatus ) = 0;
		//transfer ownership
		//@pStatus also used to link Object(owner) and Status by Status::AddListener(Object)
		//@pStatus will be nullptr after successful execution
		void AddStatus(Status* & pStatus);

		void RemoveStatus(Status*pStatus);

		virtual std::string About() const;

	protected:
		//has ownership on Status
		list<std::shared_ptr<Status>> m_pStatus;
		list<std::shared_ptr<Status>> m_pScheduledForRemoval;

		void CleanScheduledStatuses();
	};

	struct StatusImpl {
		float		m_time,
			//recharge timer
					m_recharge;
		const float m_cooldown,
			// damage	with sign + (positive value)
			// heal		with sign - (negative value)
			// damage/heal for each tick
					m_hpInfluence;
		t_Percents	m_speedInfluence;
		bool		m_isInfection;

		StatusImpl(
			float time, 
			float cd, 
			float hpInfluence, 
			t_Percents speed = 0.f,  
			bool isInfection = false
		) :
			m_time(time),
			m_recharge(0.f),
			m_cooldown(cd),
			m_hpInfluence(hpInfluence),
			m_speedInfluence(speed),
			m_isInfection(isInfection){}

	};

	//Observable
	class Status {
	public:
	
		explicit Status(int time, float cd, float hp, float speed, bool isInfection);

		virtual ~Status();

		virtual void Update(float dt);
		//allocate new Status
		//return pointer [Status*]
		virtual Status* Clone() const;

		virtual std::string About() const;

		bool IsInfection() const;

		void AddListener(Object*pObj);

		void RemoveListener();

		StatusImpl GetData()const;

	protected:

		virtual void NotifyAboutOutbreak();

		virtual void NotifyAboutExpire();

		StatusImpl	m_data;
		//don't have any ownership just links
		Object*		m_pListener; //owner
	};
}