#include "Core.h"

void Core::Object::Update(float dt)
{
	CleanScheduledStatuses();
	for (auto&s : m_pStatus) s->Update(dt);
}

/*
	Type is [Status * &] to be sure it CAN'T be modified outside
	after function's execution
*/
void Core::Object::AddStatus(Status * & pStatus)
{
	//init smart pointer with @pStatus
	std::shared_ptr<Status> ps(pStatus);
	//clear pointer outside of the function
	pStatus = nullptr;
	//now there is only one pointer to status - @ps
	
	m_pStatus.emplace_back(ps);
	//now there is 2 pointers to status - @ps & @m_pStatus.back()
	
	//let status know who is it's owner
	ps->AddListener(this);

	//after @ps.reset reference to status is only in @m_pStatus.back() 
	assert(m_pStatus.back().use_count() == 2);
	ps.reset();
	assert(m_pStatus.back().unique());
}

void Core::Object::RemoveStatus(Status * pStatus)
{
	std::shared_ptr<Status> ps = nullptr;
	for (auto&pSt : m_pStatus) {
		if (pSt.get() == pStatus) {
			ps = pSt;
			break;
		}
	}
	if (ps == nullptr) 
		throw std::logic_error("Trying to remove no-existing status!");
	
	pStatus->RemoveListener();

	assert(ps.use_count() == 2);

	m_pScheduledForRemoval.push_back(ps);

	assert(m_pScheduledForRemoval.back().use_count() == 3);
}

std::string Core::Object::About() const
{
	return "Object";
}

void Core::Object::CleanScheduledStatuses()
{
	for (auto&pSch : m_pScheduledForRemoval) {
		m_pStatus.remove(pSch);
	}
	m_pScheduledForRemoval.clear();
}

Core::Status::Status(int time, float cd, float hp, float speed, bool isInfection) :
	m_data(static_cast<float> (time), cd, hp, speed, isInfection)
{
	ILOG("ctor Status");
}

Core::Status::~Status() {
	ILOG("dtor Status");
}

void Core::Status::Update(float dt) {
	m_data.m_time -= dt;
	m_data.m_recharge -= dt;

	if (m_data.m_recharge <= 0.f) {
		this->NotifyAboutOutbreak();
		m_data.m_recharge = m_data.m_cooldown;
	}

	if (m_data.m_time <= 0.f)
		this->NotifyAboutExpire();

}

Core::Status * Core::Status::Clone() const {
	try {
		Status* ps = new Status(
			static_cast<int>(m_data.m_time), // remaining status time 
			m_data.m_cooldown,
			m_data.m_hpInfluence,
			m_data.m_speedInfluence,
			m_data.m_isInfection
		);
		return ps;
	}
	catch (std::bad_alloc&e) {
		ELOG(e.what());
		throw;
	}
}

std::string Core::Status::About() const {
	return "Abstract Status";
}

bool Core::Status::IsInfection() const {
	return m_data.m_isInfection;
}

void Core::Status::AddListener(Object * pObj) {
	m_pListener = pObj;
}

void Core::Status::RemoveListener() {
	assert(m_pListener != nullptr);
	// doesn't have ownerships so just NULL
	m_pListener = nullptr;
}

Core::StatusImpl Core::Status::GetData() const {
	return m_data;
}

void Core::Status::NotifyAboutOutbreak() {
	ILOG("call NotifyAboutOutbreak()");
	if (m_pListener != nullptr)
		m_pListener->Outbreak(this);
}

void Core::Status::NotifyAboutExpire() {
	ILOG("call NotifyAboutExpire()");
	if (m_pListener != nullptr)
		m_pListener->Expire(this);
}
