#pragma once
#include <list>
#include <vector>
#include <memory>
#include "Core.h"

using namespace Core;

class Cell: public Object {
public:
	Cell(int row, int col);

	virtual ~Cell() {}

	void AddObject(Object*pObj);

	void RemoveObject(Object*pObj);

	void Update(float dt) override;

	void Outbreak(const Status*const) override;

	void Expire( Status* ) override;

	std::string About() const override;

	void Apply(const Status * const) override;
	// pair.first  - row
	// pair.second - column
	std::pair<int, int> GetCoordinates() const;

private:
	int		m_row,
			m_col;

	//don't have any ownership just links
	std::list<Object*> m_pVisitor;

// <section>
// which option use - depends on status
	// inflect status on visiors => 
	// visitors will have it even not being on the cell
	void Spread(const Status * const pStatus);
	// only affect visitor while it's on cell (one time damage\heal)
	void Affect(const Status * const pStatus);
//	</section>
};

class Map {
public:
	enum class Size { rows = 100, cols = 75 };

	Map() {
		int rows = static_cast<int>(Size::rows),
			cols = static_cast<int>(Size::cols);

		for (int i = 0; i < rows; i++) {

			std::vector<std::shared_ptr<Cell>> vCol;

			for (int j = 0; j < cols; j++) {
				vCol.emplace_back(std::make_shared<Cell>(i,j));
			}
			assert(vCol.size() == cols);

			m_map.emplace_back(vCol);
		}
		assert(m_map.size() == rows);

		ILOG("Map ctor");
	}

	std::shared_ptr<Cell>& Get(int i, int j) {
		return m_map[i][j];
	}

	void Update(float dt) {
		int rows = static_cast<int>(Size::rows),
			cols = static_cast<int>(Size::cols);

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				m_map[i][j]->Update(dt);
			}
		}

	}

private:

	std::vector<std::vector<std::shared_ptr<Cell>>> m_map;
};

namespace MapHelper 
{
	extern bool IsWithinMap(int row, int col);

	//calculate like distance between 2 dots
	extern uint32_t CalculateDistance(int r1, int c1, int r2, int c2);
}

