#pragma once
#include "Bolts.h"
#include "Core.h"
#include <stack>
#include <memory>
using namespace Bolts;
using namespace std;

struct Stats {
	float	m_velocity;
	int		m_hp;
};

class Player : public Core::Object {
public:
	// ask boltfactory for the new Bolt 
	Bolt* AskFactory(const Token&);

	void UseBolt();
//override
	virtual void Update(float dt) override;

private:
	//our ammo
	stack<shared_ptr<Bolt>> m_bolts;
	shared_ptr<Stats>		m_stats;
};

/*
//used only by status�
virtual void Damage(int dmg) override;

virtual void Heal(int heal) override;

virtual void Freeze(Core::t_Percents reduceBy) override;

*/