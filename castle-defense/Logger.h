#pragma once
#include <iostream>
#include <fstream>
#include <string>

class Logger {
public:
	enum class Level { 
		ERROR, 
		WARNING, 
		INFO 
	};
	Logger() {}
	~Logger() {}

	void Write(const Level&lvl, const char* msg) {
		WriteToFile(lvl, msg);
		WriteToConsole(lvl, msg);
	}
private:
	const char* _filename = "log.txt";

	void WriteToFile(const Level&lvl, const char* msg) {
		std::fstream file(_filename, std::ios::binary | std::ios::out | std::ios::app);
		if (file.is_open()) {
			int cnt = static_cast<int>(lvl);
			while (cnt--) file << "  ";
			file << LevelToString(lvl) << msg << std::endl;
			file.close();
		}
	}
	void WriteToConsole(const Level&lvl, const char* msg) {
		int cnt = static_cast<int>(lvl);
		while (cnt--) std::cout << "  ";
		std::cout << LevelToString(lvl) << msg << std::endl;
	}

	std::string LevelToString(const Level& lvl) {
		std::string slvl;
		switch (lvl) {
			case Level::ERROR:	slvl = "ERROR: "; break;
			case Level::WARNING:slvl = "WARNING: "; break;
			case Level::INFO:	slvl = "INFO: "; break;
			default: slvl = "broken"; break;
		}
		return slvl;
	}
};

class Log {
	Logger	mLogger;
	Log() = default;
	Log(const Log&) = delete;
	Log& operator=(Log&) = delete;
	Log(Log&&) = delete;
	Log& operator=(Log&&) = delete;
public:
	static Log& GetInstance() {
		static Log instance;
		return instance;
	}
	void Write(const Logger::Level&lvl, const char* msg) {
		mLogger.Write(lvl, msg);
	}
};

#ifdef ENABLE_LOGGING
//								*** DEFINES ***
#define ELOG(msg) Log::GetInstance().Write(Logger::Level::ERROR,	(msg));
#define WLOG(msg) Log::GetInstance().Write(Logger::Level::WARNING,	(msg));
#define ILOG(msg) Log::GetInstance().Write(Logger::Level::INFO,		(msg));

#else 
//								*** DEFINES ***
#define ELOG(msg) ;
#define WLOG(msg) ;
#define ILOG(msg) ;


#endif