#include "Effects.h"

using namespace BoltEffect::UniqueStatus;


BoltEffect::Explosion::Explosion(
	uint32_t radius,
	float dmgInCntr
) :
	m_radius(radius),
	m_damageInEpicenter(dmgInCntr) {}

float BoltEffect::Explosion::CalculateDamage(uint32_t targetRange) const
{
	if (m_radius < 0 || targetRange < 0 || targetRange > m_radius)
		throw std::logic_error("Wrong range input");
	//linear function by default
	return (m_radius - targetRange) / m_radius * m_damageInEpicenter;
}

void BoltEffect::Explosion::Start(std::shared_ptr<Cell>& pCell, Map&map)
{
	std::pair<int, int> center(pCell->GetCoordinates());

	auto AddStatusToCell = [this,&center](std::shared_ptr<Cell>& cell) {
		std::pair<int, int> point = cell->GetCoordinates();
		Status* status = this->CreateStatus(
			MapHelper::CalculateDistance(center.first,center.second, point.first, point.second)
		);	//@status own resources now
		cell->AddStatus(status);	
		assert(status == nullptr);	//@status is free now
	};
// Spread algo: add status for all cells in explosion zone
	int step = 1;
	//1. Going towards top from bottom 
	while (step <= m_radius) {
		int width = step * 2 - 1;
		int r = center.first - m_radius + step,
			c = center.second;
		if (MapHelper::IsWithinMap(r,c)) {
			//2. Used middle cell
			AddStatusToCell(map.Get(r, c));
			//3. Going left and right
			int usedCells = 1,
				iterations = 0; 
			while (usedCells < width) {
				iterations++;

				if (MapHelper::IsWithinMap(r, c + iterations))
					AddStatusToCell(map.Get(r, c + iterations));
				if (MapHelper::IsWithinMap(r, c - iterations))
					AddStatusToCell(map.Get(r, c - iterations));

				usedCells += 2;
			}
		}
		step++;
	}
	//4. Going bottom from middle
	step = m_radius - 1;
	while ( step >= 0 ) {
		int width = step * 2 - 1;
		int r = center.first + m_radius - step,
			c = center.second;
		if (MapHelper::IsWithinMap(r, c)) {
			//5. Used middle cell
			AddStatusToCell(map.Get(r, c));
			//6. Going left and right
			int usedCells	= 1,
				iterations	= 0;
			while (usedCells < width) {
				iterations++;

				if (MapHelper::IsWithinMap(r, c + iterations))
					AddStatusToCell(map.Get(r, c + iterations));
				if (MapHelper::IsWithinMap(r, c - iterations))
					AddStatusToCell(map.Get(r, c - iterations));

				usedCells += 2;
			}
		}
		step--;
	}
//endl algo
}


BoltEffect::FireExplosion::FireExplosion(
	uint32_t radius,
	float dmgInCntr, 
	float duration
) :
	Explosion(radius, dmgInCntr),
	m_duration(duration) {}

float BoltEffect::FireExplosion::CalculateDamage(uint32_t targetRange) const
{
	if (m_radius < 0 || targetRange < 0 || targetRange > m_radius)
		throw std::logic_error("Wrong range input");
	//constant function by default
	return m_damageInEpicenter;
}

void BoltEffect::FireExplosion::Start(std::shared_ptr<Cell>& pCell, Map&map)
{
	//TODO: Change strategy 
	//temporary use standart and simplest one from basic class
	Explosion::Start(pCell, map);
}

Status* BoltEffect::FireExplosion::CreateStatus(uint32_t targetRange) const
{
	try {
		float dmg = this->CalculateDamage( targetRange );
		BurningStatus* status = new BurningStatus( m_duration, 0.5f, dmg, false );
		return status;
	}
	catch (std::bad_alloc &e) {
		ELOG(e.what());
		throw;
	}
}

BurningStatus::BurningStatus(
	int time, 
	float cd, 
	float dmg, 
	bool isInfection
) :
	Status(time, cd, dmg, 0.f, isInfection) 
{}

std::string BurningStatus::About() const {
	return "Burning time: "+ std::to_string(m_data.m_time);
}
