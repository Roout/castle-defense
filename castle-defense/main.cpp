#include "Core.h"
#include "Map.h"
#include "Effects.h"
#define ENABLE_LOGGING

#include <vector>
int main() {
	
	Map map;
	enum {a,b,c};
	BoltEffect::FireExplosion* m_explosion = new BoltEffect::FireExplosion(a, b, c);
	m_explosion->Start(map.Get(1, 1), map);
	int i = 0;
	while (true) {
		getchar();
		std::cout << "Cycle " << i++ << std::endl;
		map.Update(1);
	}
	
	
	return 0;
}