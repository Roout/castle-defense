Castle Defense
==
1.	Map size
-
<p>Size of the whole map is not defined yet. There will be only visible part.</p>
2.	Game mode
-
<p>Single, Multi-user (later): Castle against castle, I guess, with some monsters.</p>
3.	Platform
-
<p>Don’t know. Should be mobile (iOS, Android); Why? Because it’s easy publish.</p>
4.	Small introduction for the game
-
<p>Spread, defend and upgrade your caste. 
Don’t give any chance to monsters. 
Stand against hordes of strange cruel creatures. </p>
5.	Genres & Tags
-
<p>Strategic, Tower defense, Economic, Evolution</p>
6.	Conception
-
<p>Middle Ages maybe with guns, cannons, towers, gunpowder.</p>
7.	Some main points
-
<p>You’re the owner of the castle. In brief you will manage economic, military and other spheres of life.</p>
<p>There will be bosses too.  
<p>You usually won’t know where the next wave will come from. It’s possible it will be from all directions. Hints can be seen from information of yours people.  </p>
<p>Killing monsters can give you a huge reward. Tails, fur, feather – almost all can be used for enhancing the economic.  </p>
<p>To kill one bunch of monsters can be enough to dig a pit before place with bait (some food, people, f.e. soldier). Some will go around the pit. </p> 
<p>It’s important to know who your opponent in the next wave is. You will have different sighs to get some points of their behavior like below:</p>
* Attention farmers have seen some strange eyes in forest from the west.  
// it’s highly possible that next wave will be from the West.  
* Hunters have seen a lot of dead bodies of small animals. They are without blood.  
// nearly 90% that next wave is connected with vampires. Prepare silver for defense.
Etc.  
8.	 Phases
-
<p>Peace – Preparations – War</p>
9.	Peace 
-
<p>How detail should it be?</p>
10.	Preparations
-
<p>How detail should it be?</p>
11.	War
-
<p>How detail should it be?</p>
  
**Trello:**  <https://trello.com/b/F2SI9ukt/castle-defense>
