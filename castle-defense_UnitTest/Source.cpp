#include <gtest\gtest.h>
#include "../castle-defense/Core.h"
#include "../castle-defense/Map.h"
#include "../castle-defense/Effects.h"

class CellTest : public ::testing::Test {
public:
	CellTest() {
		m_pStatus = new Core::Status(5, 2, 0, 0, true);
	}
	virtual ~CellTest() {
		if (m_pStatus != nullptr) delete m_pStatus;
	}
	virtual void SetUp() override {

	}
	virtual void TearDown() override {

	}

	Map m_map;
	Core::Status* m_pStatus;
};

class ExplosionTest : public ::testing::Test {
public:
	enum {RADIUS = 5, DAMAGE = 10, DURATION = 10, POINTS_COUNT = 8};

	ExplosionTest() {
		m_explosion = new BoltEffect::FireExplosion(RADIUS, DAMAGE, DURATION);
		// points  near the border
		m_points[0] = { 1, 1 };
		m_points[1] = { 
			static_cast<int>(Map::Size::rows) - 1, 
			static_cast<int>(Map::Size::cols) - 1 
		};
		m_points[2] = { 0, static_cast<int>(Map::Size::cols) - 1 };
		m_points[3] = { static_cast<int>(Map::Size::rows) - 1, 0 };
		// normal points
		m_points[4] = { 50, 50 };
		m_points[5] = { 20, 20 };
		// overlapping with same status
		m_points[6] = { 21, 21 };
		m_points[7] = { 22, 21 };

		for (int i = 0; i < POINTS_COUNT; i++) {
			m_cells[i] = m_map.Get(m_points[i].first, m_points[i].second);
			m_explosion->Start(m_cells[i], m_map);
		}
	}
	virtual ~ExplosionTest() {
		if (m_explosion != nullptr)
			delete m_explosion;
	}
	virtual void SetUp() override {
	
	}
	virtual void TearDown() override {

	}
	int GetStatusCount(std::pair<int, int>&&p) {
		std::string about = m_map.Get(p.first, p.second)->About();
		auto& it = std::find(about.begin(), about.end(), '(');
		if (it == about.end()) return 0;
		else {
			it++;//Statuses(1
			std::string count;
			while (*it != ')') {
				count.push_back(*it);
				it++;
			}
			return std::stoi(count);
		}
	}
protected:
	Map							m_map;
	BoltEffect::FireExplosion*	m_explosion;
	std::pair<int,int>			m_points[POINTS_COUNT];
	std::shared_ptr<Cell>		m_cells[POINTS_COUNT];
};

TEST(CloneStatusFn, CreateCloneWithRemainingDuration) 
{
	Core::Status*source = new Core::Status(5, 2, 0, 0, true);

	source->Update(2.f);
	source->Update(2.f);

	Core::Status*clone = source->Clone();

	EXPECT_EQ(source->GetData().m_time, clone->GetData().m_time)<<" Clone's status DOESN't have same duration";
	EXPECT_EQ(clone->GetData().m_time, 1.f) << " Clone's status duration isn't 1 second";


	delete source;
	delete clone;
}

TEST(Map, AllCellsAreWithingMap)
{
	Map map;
	for (int i = 0; i < static_cast<int> (Map::Size::rows); i++) {
		for (int j = 0; j < static_cast<int> (Map::Size::cols); j++) {
			EXPECT_TRUE(MapHelper::IsWithinMap(i, j)) << "Cell isn't within map: " << i << "," << j;;
		}
	}
}

TEST_F(CellTest, AddStatusClearPassedPointer)
{
	m_map.Get(1, 1)->AddStatus(m_pStatus);

	EXPECT_EQ(m_pStatus, nullptr) << "Failed clear @m_pStatus.";
}

TEST_F(CellTest, RemoveForeignStatusWithLogicError)
{
	ASSERT_THROW(m_map.Get(1, 1)->RemoveStatus(m_pStatus), std::logic_error);
}

TEST_F(ExplosionTest, ExplodedCellsHasBurningStatus)
{
	for (int i = 0; i < POINTS_COUNT; i++) {
		std::pair<int, int> center(m_points[i]);

		// Spread algo: add status for all cells in explosion zone
		int step = 1;
		//1. Going towards top from bottom 
		while (step <= RADIUS) {
			int width = step * 2 - 1;
			int r = center.first - RADIUS + step,
				c = center.second;
			if (MapHelper::IsWithinMap(r, c)) {
				//2. Check middle cell
				ASSERT_GE(GetStatusCount({ r, c }), 1) << "Cell Without statuses: GetStatusCount == " << GetStatusCount({ r, c });
				//3. Going left and right
				int usedCells = 1,
					iterations = 0;
				while (usedCells < width) {
					iterations++;

					if (MapHelper::IsWithinMap(r, c + iterations))
						ASSERT_GE(GetStatusCount({ r, c + iterations }), 1) << "Cell Without statuses: GetStatusCount == " << GetStatusCount({ r,c + iterations });
					if (MapHelper::IsWithinMap(r, c - iterations))
						ASSERT_GE(GetStatusCount({ r, c - iterations }), 1) << "Cell Without statuses: GetStatusCount == " << GetStatusCount({ r, c - iterations });

					usedCells += 2;
				}
			}
			step++;
		}
		//4. Going top from middle
		step = RADIUS - 1;
		while (step >= 0) {
			int width = step * 2 - 1;
			int r = center.first + RADIUS - step,
				c = center.second;
			if (MapHelper::IsWithinMap(r, c)) {
				//5. Check middle cell
				ASSERT_GE(GetStatusCount({ r, c }), 1) << "Cell Without statuses: GetStatusCount == "<< GetStatusCount({ r, c });
				//6. Going left and right
				int usedCells = 1,
					iterations = 0;
				while (usedCells < width) {
					iterations++;

					if (MapHelper::IsWithinMap(r, c + iterations))
						ASSERT_GE(GetStatusCount({ r, c + iterations }), 1) << "Cell Without statuses: GetStatusCount == " << GetStatusCount({ r,c + iterations });
					if (MapHelper::IsWithinMap(r, c - iterations))
						ASSERT_GE(GetStatusCount({ r, c - iterations }), 1) << "Cell Without statuses: GetStatusCount == " << GetStatusCount({ r, c - iterations });

					usedCells += 2;
				}
			}
			step--;
		}
	}
}

int main(int argc, char**argv) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}